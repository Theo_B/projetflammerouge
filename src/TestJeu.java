import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestJeu {
	/**
	 * test la phase de mouvement 
	 */
	@Test
	public void testphaseMouvement() {
		Jeu j=new Jeu(new Plateau(),2);
		assertEquals("le cycliste ne devrait pas etre arrive(phase de mouvement)",false,j.phaseMouvement());
	}
	/**
	 * test que le cycliste n'est pas arriver a la fin
	 */
	@Test
	public void testphaseMouvement_etreArrive_f() {
		Jeu j=new Jeu(new Plateau(),2);
		Case c=new Case(111);
		assertEquals("le cycliste ne devrait pas etre arrive",false,j.etreArrive(new Cycliste(1,new Joueur(),c)));
	}
	/**
	 * test que le cycliste est arriv� a la fin
	 */
	@Test
	public void testphaseMouvement_etreArrive_t() {
		Jeu j=new Jeu(new Plateau(),2);
		Case c=new Case(222);
		assertEquals("le cycliste devrait etre arrive",true,j.etreArrive(new Cycliste(1,new Joueur(),c)));
	}
	
}
