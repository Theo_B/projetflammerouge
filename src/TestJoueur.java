import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

public class TestJoueur {
	/***
	 * test du constructeur de joueur
	 */
	@Test
	public void testConstructeurJoueur() {//test nb joueur,couleur
		Plateau p=new Plateau();
		Joueur j = new Joueur(1,Color.blue,"pat",p);
		assertEquals("", 1,j.getNum());
		assertEquals("", Color.blue,j.getCouleur());
		assertEquals("","pat",j.getNom());
		assertEquals("", p,j.getPt());
	} 
	//public void test
}
