import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

public class TestCase {
	/**
	 * test le constructeur de Case
	 */
	@Test
	public void testConstructeurCase() {
		Case c = new Case(2);
		assertEquals("La position devrait etre egale a 0",0,c.getPosi());
		assertEquals("le cycliste de gauche devrait etre null",null,c.getCyG());
		assertEquals("le cycliste de droite devrait etre null",null,c.getCyD());
		assertEquals("effet devrait etre egale a 2",2,c.getEffet());
	}
	/**
	 * test le deplacement d'un cycliste quand la case devant lui est entierement null
	 */
	@Test
	public void testplacerCycliste_null() {
		Case c = new Case(0);
		Plateau p=new Plateau();
		boolean res;
		res=c.placerCycliste(new Cycliste(1, new Joueur(1, Color.blue, "Jean", p),c));
		assertEquals("le cycliste devrait avancer",true,res);

	}
	/**
	 * test le deplacement d'un cycliste quand la place droite de la case est null
	 */
	@Test
	public void testplacerCycliste_nullD() {
		Case c = new Case(0);
		Plateau p=new Plateau();
		boolean res;
		c.setCyG(new Cycliste(2, new Joueur(2, Color.red, "Peter", p),c));
		res=c.placerCycliste(new Cycliste(1, new Joueur(1, Color.blue, "Jean", p),c));

		assertEquals("le cycliste devrait avancer sur la droite",true,res);
	}
	/**
	 * test le deplacement d'un cycliste quand la place gauche de la case est null
	 */
	@Test
	public void testplacerCycliste_nullG() {
		Case c = new Case(0);
		Plateau p=new Plateau();
		boolean res;
		c.setCyD(new Cycliste(2, new Joueur(2, Color.red, "Peter", p),c));
		res=c.placerCycliste(new Cycliste(1, new Joueur(1, Color.blue, "Jean", p),c));
		
		assertEquals("le cycliste devrait avancer sur la gauche",true,res);
	}
	/**
	 * test le deplacement d'un cycliste dans le cas ou la case devant lui est occup�e
	 */
	@Test
	public void testplacerCycliste_false() {
		Case c = new Case(0);
		Plateau p=new Plateau();
		boolean res;
		c.setCyD(new Cycliste(2, new Joueur(2, Color.red, "Peter", p),c));
		c.setCyG(new Cycliste(3, new Joueur(3, Color.green, "Jack", p),c));
		res=c.placerCycliste(new Cycliste(1, new Joueur(1, Color.blue, "Jean", p),c));
		
		assertEquals("le cycliste devrait avancer sur la gauche",false,res);
	}
	
}
