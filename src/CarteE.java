/**
 * Class de Carte Energie
 * @author Theo_B & Geoff_M
 */
public class CarteE extends Carte{
	private int deplacement;
	private int typeCycliste;
	
	/**
	 * Constructeur de carte energie
	 * @param val : valeur de deplacement de la carte
	 * @param tc  : type du cycliste : 1=Sprinter / 2=Rouleur
	 */
	public CarteE(int val, int tc){
		this.deplacement=val;
		this.typeCycliste=tc;
	}
	
	/**
	 * Getter TypeCycliste
	 * @return int typeCycliste :
	 * 			 1=Sprinter
	 * 			 2=Rouleur
	 */
	public int getTypeCoureur() {
		return typeCycliste;
	}
	
	/**
	 * Getter Deplacement
	 * @return int deplacement 
	 */
	public int getDeplacement() {
		return deplacement;
	}

	
	@Override
	public String toString() {
		return "CarteE [deplacement=" + deplacement + "]";
	}
	
	
}
