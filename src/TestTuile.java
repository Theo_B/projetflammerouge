import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestTuile {
	/**
	 * test du constructeur de tuile
	 */
	@Test
	public void testConstructeurTuile() {
		Tuile t=new Tuile("Depart");
		Tuile t1=new Tuile("Arrivee");
		Tuile t2=new Tuile("VirageLG");
		Tuile t3=new Tuile("VirageLD");
		Tuile t4=new Tuile("VirageSD");
		Tuile t5=new Tuile("VirageSG");
		Tuile t6=new Tuile("Droit");
		
		assertEquals("le type de la tuile t n'est pas valide","Depart",t.getTypeTuile());
		assertEquals("le type de la tuile t1 n'est pas valide","Arrivee",t1.getTypeTuile());
		assertEquals("le type de la tuile t2 n'est pas valide","VirageLG",t2.getTypeTuile());
		assertEquals("le type de la tuile t3 n'est pas valide","VirageLD",t3.getTypeTuile());
		assertEquals("le type de la tuile t4 n'est pas valide","VirageSD",t4.getTypeTuile());
		assertEquals("le type de la tuile t5 n'est pas valide","VirageSG",t5.getTypeTuile());
		assertEquals("le type de la tuile t6 n'est pas valide","Droit",t6.getTypeTuile());
	}
	
}
