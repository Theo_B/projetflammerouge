import java.util.ArrayList;
import java.util.Random;


public class Plateau {

	public static int TP=17;				//Taille du plateau
	private ArrayList<Tuile> listetuile;	//liste de tuile disponible
	private ArrayList<Tuile> circuit;		//liste de tuile melanger
	private ArrayList<Case> listePos;		//liste de position de case

	/**
	 * Constructeur par defaut de Plateau
	 */
	public Plateau(){
		this.listetuile=new ArrayList<Tuile>();
		this.circuit=new ArrayList<Tuile>();
		for(int i=0;i<=7;i++){
			listetuile.add(creationTuile(0));
			if(i>=1&&i<=4){
				listetuile.add(creationTuile(i));
				listetuile.add(creationTuile(i));
			}
		}

		melangeTuile();
		circuit.add(0,creationTuile(5));
		circuit.add(creationTuile(6));

		listePos = new ArrayList<Case>();

		Case ctmp;
		Tuile ttmp;
		int npos =0 ;
		for (int i = 0; i < circuit.size(); i++) {
			ttmp=circuit.get(i);
			for (int j = 0; j < ttmp.getChausse().length; j++) {
				ttmp.getChausse()[j].setPosi(npos);
				listePos.add(ttmp.getChausse()[j]);
				npos++;
			}
		}

	}

	/**
	 * methode de melange de tuile (pour creer un circuit)
	 */
	public void melangeTuile(){
		for (int i = 0; i < 15; i++) {
			this.circuit.add(null);
		}
		int indicetmp;
		for(int i=0;i<15;i++){
			indicetmp=(int)(Math.random()*15);
			
			while(this.circuit.get(indicetmp)!=null){
				indicetmp=(int)(Math.random()*15);
				
			}
			this.circuit.set(indicetmp, this.listetuile.get(i));
			
		}
	}

	/**
	 * methode d'affichage du plateau
	 */
	public void affichePlat(){
		for(int i=0;i<TP;i++){
			System.out.println(this.circuit.get(i));
		}
	}

	/**
	 * methode de creation de tuile
	 * @param i le type de tuile sous forme d'entier
	 * @return
	 */
	public Tuile creationTuile(int i){
		Tuile res=null;

		if(i==0)res=new Tuile("Droit");
		if(i==1)res=new Tuile("VirageLG");
		if(i==2)res=new Tuile("VirageLD");
		if(i==3)res=new Tuile("VirageSD");
		if(i==4)res=new Tuile("VirageSG");
		if(i==5)res=new Tuile("Depart");
		if(i==6)res=new Tuile("Arrivee");
		return res;
	}

	/**
	 * le boolean asp est faux si la methode est 
	 * appelee par un deplacement d'aspiration
	 * (les effets ne s'applique alors pas)
	 * @param c
	 * @param d
	 */
	public void Deplacement(Cycliste c,int d,boolean asp){
		int i = 0;
		while(listePos.get(i).getId()!=c.getPos().getId()) {
			i++;
		}
		if(!asp) {
		d=AppliquerEffet(listePos.get(i), d);
		}
		int ni = i+d;

		if(c.getPos().getCyD()==c) {
			listePos.get(i).setVideD();
		}
		else {
			listePos.get(i).setVideG();
		}
		try {
			if(listePos.get(ni).placerCycliste(c)) {
				
			}
			else {
				while(!listePos.get(ni).placerCycliste(c)) {
					ni--;
				}
			}
		}
		catch(IndexOutOfBoundsException e) {
			listePos.get(listePos.size()-1).placerCycliste(c);
		}

	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Case> caseAvecCycliste() {
		ArrayList<Case> res = new ArrayList<Case>();
		for (int i = 0; i < listePos.size(); i++) {
			if(listePos.get(i).estVideG()==false) {
				res.add(listePos.get(i));
			}

			if(listePos.get(i).estVideD()==false) {
				res.add(listePos.get(i));
			}

		}

		return res;
	}


	public void setListePos(ArrayList<Case> listePos) {
		this.listePos = listePos;
	}



	public ArrayList<Case> getListePos() {
		return listePos;
	}

	public ArrayList<Tuile> getListetuile() {
		return listetuile;
	}

	public ArrayList<Tuile> getCircuit() {
		return circuit;
	}

	
	public int AppliquerEffet(Case c,int d) {
		int res=d;
		int effet = c.getEffet();
		switch (effet) {
		case 1:
			if(res>5) {
				res=5;
			}
			break;

		case 2:
			if(res<5) {
				res=5;
			}
	
			break;
			
		case 3:
			Random random = new Random();
			int foule = random.nextInt(2);
			if(foule==0) {
				res++;
			}
			else {
				res--;
			}
	
			break;

		default:
			break;
		}


		return res;
	}

}
