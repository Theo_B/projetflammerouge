import java.util.Random;

/**
 * Class Tuile
 * @author Theo_B & Geoff_M
 */
public class Tuile {
	private String typeTuile;
	private Case[] chausse;
	
	/**
	 * Constructeur de Tuile
	 * @param type : String : chaine correspondant au type de la Tuile
	 */
	public Tuile (String type){
		int effetAlea = randomEffet();
		if(type.equals("Depart")){
			this.typeTuile=type;
			this.chausse=new Case[6];
			this.chausse[5]=new Case(0);
			this.chausse[4]=new Case(0);
			for (int i = 0; i < chausse.length-2; i++) {
				this.chausse[i]=new Case(111);
			}
		}
			if(type.equals("Arrivee")){
				this.typeTuile=type;
				this.chausse=new Case[6];
				this.chausse[0]=new Case(0);
				this.chausse[1]=new Case(0);
				for (int i = 2; i < chausse.length; i++) {
					this.chausse[i]=new Case(222);
				}
		}
		
			if(type.equals("VirageLG")){
				this.typeTuile=type;
				this.chausse=new Case[2];
				for (int i = 0; i < chausse.length; i++) {
					this.chausse[i]=new Case(effetAlea);
				}
		}
			
			if(type.equals("VirageLD")){
				this.typeTuile=type;
				this.chausse=new Case[2];
				for (int i = 0; i < chausse.length; i++) {
					this.chausse[i]=new Case(effetAlea);
				}
		}
		
			if(type.equals("VirageSD")){
				this.typeTuile=type;
				this.chausse=new Case[2];
				for (int i = 0; i < chausse.length; i++) {
					this.chausse[i]=new Case(effetAlea);
				}
		}
			
			if(type.equals("VirageSG")){
				this.typeTuile=type;
				this.chausse=new Case[2];
				for (int i = 0; i < chausse.length; i++) {
					this.chausse[i]=new Case(effetAlea);
				}
		}
			if(type.equals("Droit")){
				this.typeTuile=type;
				this.chausse=new Case[6];
				for (int i = 0; i < chausse.length; i++) {
					this.chausse[i]=new Case(effetAlea);
				}
		}

	}


	public String getTypeTuile() {
		return typeTuile;
	}


	public Case[] getChausse() {
		return chausse;
	}


	@Override
	public String toString() {
		return typeTuile;
	}
	/**
	 * methode de position d'une case
	 * @param c : Case : la case rechercher
	 * @return res : int : entier correspondant a l'emplacement de la case
	 */
	public int getIndexCase(Case c){
		int res=0;
		while(c!=this.chausse[res]){
			res++;
		}
		return res;
	}
	
	
	public int randomEffet() {
		Random rd =new Random();
	
		int res = rd.nextInt(6);
		if(res==5 || res==4) {
			res=0;
		}
		
		return res;
	}
}
