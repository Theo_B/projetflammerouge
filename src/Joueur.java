/**
 * Class Joueur
 * @author Theo_B & Geoff_M
 */
import java.awt.Color;

public class Joueur {
	int num;
	private String nom;
	private Color couleur;
	private Plateau pt;
	private Cycliste sprinter,rouleur;
	/**
	 * Constructeur de Joueur
	 * @param i : int : numero du joueur
	 * @param c : Color : couleur du joueur
	 * @param n : String : nom du joueur
	 * @param p : Plateau : plateau sur lequel se trouve le(s) joueur(s)
	 */
	public Joueur(int i,Color c,String n,Plateau p) {
		this.num=i;
		this.nom=n;
		this.couleur=c;
		this.pt=p;
		this.sprinter=new Cycliste(1, this);
		this.rouleur=new Cycliste(2,this);
		
	}
	/**
	 * Constructeur utiliser pour les tests
	 */
	public Joueur() {
		this.num=1;
		this.nom="jean";
		this.couleur=Color.GRAY;
		this.pt=new Plateau();
		this.sprinter=new Cycliste(1, this);
		this.rouleur=new Cycliste(2,this);
	}

	public String getNom() {
		return nom;
	}

	public Color getCouleur() {
		return couleur;
	}

	public Plateau getPt() {
		return pt;
	}

	public Cycliste getSprinter() {
		return sprinter;
	}

	public Cycliste getRouleur() {
		return rouleur;
	}

	public int getNum() {
		return num;
	}
	
	public String toString() {
		return nom;
	}
	
}
