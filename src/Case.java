/**
 * Class Case
 * @author Theo_B & Geoff_M
 */
public class Case {
	private Cycliste cyD;//cycliste de la partie droite
	private Cycliste cyG;//cycliste de la partie gauche
	private int effet;
	int posi;
	int id;
	
	
	/**
	 * Constructeur de Case
	 * 
	 * @param e : effet de la case
	 * 
	 * 0 : pas d'effet
	 * 1 : montee
	 * 2 : descente
	 * 3 : Foule  //la foule encourage ou hue le cycliste(aleatoir) lui faisant gagner ou perde un point de deplacement
	 * 111 : depart
	 * 222 : arrivee
	 * 
	 */
	public Case(int e){
		this.cyD=null;
		this.cyG=null;
		this.effet=e;
		id=(int)(Math.random()*99999999);
		posi=0;
	}



	public int getId() {
		return id;
	}


	public int getPosi() {
		return posi;
	}


	public void setPosi(int id) {
		this.posi = id;
	}


	public void setVideD() {
		this.cyD=null;
	}

	public void setVideG() {
		this.cyG = null;
	}
	
		

	/**
	 * methode : etat de la partie droite de la case
	 * @return res : 
	 * 			true si cyD = null
	 * 			false si cyD !=null
	 */
	public boolean estVideD(){
		boolean res=false;
		if(cyD==null) {
			res=true;
		}	
		return res;
	}
	
	/**
	 * methode : etat de la partie gauche de la case
	 * @return boolean res : 
	 * 			true si cyG = null
	 * 			false si cyG !=null
	 */
	public boolean estVideG(){
		boolean res=false;
		if(cyG==null) {
			res=true;
		}
			
		return res;
	}
	/**
	 * methode : retourne l'etat de la case (partie droite & gauche)
	 * @param c : Cycliste qui souhaite ce placer sur la case
	 * @return boolean res :
	 * 			true si le Cycliste a pu changer sa position
	 * 			false si le Cycliste n'a pas pu changer sa position
	 */
	public boolean placerCycliste(Cycliste c) {
		boolean res=false;
		
		if(cyD==null) {
			cyD=c;
			c.changerPos(this);
			res=true;
		}
		else {
			if(cyG==null) {
				cyG=c;
				c.changerPos(this);
				res=true;
			}

		}
		return res;
	}
	
	
	
	public Cycliste getCyD() {
		return cyD;
	}


	public Cycliste getCyG() {
		return cyG;
	}


	public int getEffet() {
		return effet;
	}


	@Override
	public String toString() {
		return "Case pos :"+posi+" cyD : "+cyD+" cyG : "+cyG+" id : "+id+"\n";
	}


	public void setCyD(Cycliste cyD) {
		this.cyD = cyD;
	}


	public void setCyG(Cycliste cyG) {
		this.cyG = cyG;
	}

	
	
}
