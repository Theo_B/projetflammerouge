/**
 * Class de Carte Fatigue
 * @author Theo_B & Geoff_M
 */
public class CarteF extends Carte{
	private int deplacement;
	
	/**
	 * Constructeur par default de carte fatigue
	 */
	public CarteF(){
		this.deplacement=2;
	}

	public int getDeplacement() {
		return deplacement;
	}
	
	public String toString() {
		return "CarteF [deplacement=" + deplacement + "]";
	}
}
