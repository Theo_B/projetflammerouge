/**
 * Class Cycliste
 * @author Theo_B & Geoff_M
 */
import java.util.*;

public class Cycliste {
	List<Carte> pioche = new ArrayList<Carte>();
	List<Carte> defausse = new ArrayList<Carte>();
	private Joueur j;
	private int typeCycliste;
	private Case pos;
		/**
		 * Constructeur de Cycliste
		 * @param tc : entier correspondant au type de cycliste :
		 * 			1=sprinter
		 * 			2=rouleur
		 * @param pj : Joueur a qui est attribuer le cycliste 
		 * 
		 */
	public Cycliste(int tc,Joueur pj) {
		this.typeCycliste=tc;
		this.j=pj;
		this.pos=null;
		initialiser_pioche(tc);
		melanger_pioche();
	}
	
	/**
	 * Constructeur test de Cycliste 
	 * @param tc : entier correspondant au type de cycliste :
	 * 			1=sprinter
	 * 			2=rouleur
	 * @param pj : Joueur a qui est attribuer le cycliste 
	 * @param c : Case : case ou va se placer le cycliste
	 */
	//ce constructeur param�tr� avec une case n'est utilis� que dans les tests
	public Cycliste(int tc,Joueur pj,Case c) {
		this.typeCycliste=tc;
		this.j=pj;
		this.pos=null;
		initialiser_pioche(tc);
		melanger_pioche();
		changerPos(c);
	}
	/**
	 * Constructeur utilise pour les tests
	 */
	public Cycliste() {
		this.typeCycliste=1;
		this.j=new Joueur();
		this.pos=null;
		
	}
	
	public ArrayList<Carte> getPioche() {
		return (ArrayList<Carte>)pioche;
	}

	/**
	 *methode d'initialisation de pioche 
	 * @param tc2
	 */
	public void initialiser_pioche(int tc2) {
		if(tc2==1) {
			this.pioche.add(new CarteE(2,1));
			this.pioche.add(new CarteE(2,1));
			this.pioche.add(new CarteE(2,1));
			
			this.pioche.add(new CarteE(3,1));
			this.pioche.add(new CarteE(3,1));
			this.pioche.add(new CarteE(3,1));
			
			this.pioche.add(new CarteE(4,1));
			this.pioche.add(new CarteE(4,1));
			this.pioche.add(new CarteE(4,1));

			this.pioche.add(new CarteE(5,1));
			this.pioche.add(new CarteE(5,1));
			this.pioche.add(new CarteE(5,1));
			
			this.pioche.add(new CarteE(9,1));
			this.pioche.add(new CarteE(9,1));
			this.pioche.add(new CarteE(9,1));
		}
		
		if(tc2==2) {
			this.pioche.add(new CarteE(3,2));
			this.pioche.add(new CarteE(3,2));
			this.pioche.add(new CarteE(3,2));
			
			this.pioche.add(new CarteE(4,2));
			this.pioche.add(new CarteE(4,2));
			this.pioche.add(new CarteE(4,2));
			
			this.pioche.add(new CarteE(5,2));
			this.pioche.add(new CarteE(5,2));
			this.pioche.add(new CarteE(5,2));
			
			this.pioche.add(new CarteE(6,2));
			this.pioche.add(new CarteE(6,2));
			this.pioche.add(new CarteE(6,2));
			
			this.pioche.add(new CarteE(7,2));
			this.pioche.add(new CarteE(7,2));
			this.pioche.add(new CarteE(7,2));
		}

	}
	
	/**
	 * melange al�atoire de la pioche 
	 */
	/**public void melanger_pioche() {
		List<Carte> tmp = new ArrayList<Carte>();
		int l = this.pioche.size();
		for (int i = 0; i <l; i++) {
			Carte c= this.pioche.get((int)(Math.random()*l));
			System.out.println("tmp : " +tmp);
			while(tmp.contains(c)) {
				//System.out.println("dans le while c= "+c);
				c= this.pioche.get((int)(Math.random()*l));
			}
			//System.out.println("sortie du while" +c);
			tmp.add(c);
		}
		pioche =tmp;
	}**/
	
	/**
	 * methode de melange al�atoir de la pioche
	 */
	public void melanger_pioche() {
		
		List<Carte> tmp = new ArrayList<Carte>();
		int l = this.pioche.size();
		for (int i = 0; i <l; i++) {
			int r = (int)(Math.random()*(l-i));
			Carte c= this.pioche.get(r);
			pioche.remove(r);
			
			
			tmp.add(c);
		}
		pioche =tmp;
		
	}
	/**
	 * methode : permet le choix des carte au joueur sur le cycliste this
	 * @return Carte : la carte choisie par le joueur 
	 */
	public Carte choixCarte() {
		Carte choix=null;
		if(this.pioche.size()>=4) {
			choix=tirage4Carte();
			
		}
		else {
		if(this.pioche.size()<4 && this.defausse.size()+this.pioche.size()>=4) {
			System.out.println("pas de cartes restante dans la pioche mais d�fausse suffisament fournis pour un melange ");
			melange_def_pio();
			choix=tirage4Carte();
		}
		if(this.pioche.size()+this.defausse.size()<4) {
			melange_def_pio();
			choix=tirageMoinde4();
		}
			
		}
		return choix;
	}

	public void setPos(Case pos) {
		this.pos = pos;
	}
	
	/**
	 * methode de melange de la defausse pioche
	 */
	public  void melange_def_pio() {
		
		for(int i =0;i<this.defausse.size();i++){
			this.pioche.add(this.defausse.get(i));
			
		}
		this.defausse.clear();
		
		melanger_pioche();
		
		
	}
	/**
	 * methode : permet le tirage d'une carte sur 4
	 * @return Carte : la carte tirer par le joueur sur les 4 propos�es
	 */
	public Carte tirage4Carte() {
		Carte c1,c2,c3,c4,choix;
		
		int cc;
		Scanner sc = new Scanner(System.in);
		c1=this.pioche.get(0);
		c2=this.pioche.get(1);
		c3=this.pioche.get(2);
		c4=this.pioche.get(3);
		
		System.out.println("les quatre carte piocher sont :");
		System.out.print(c1+" | ");
		System.out.print(c2+" | ");
		System.out.print(c3+" | ");
		System.out.println(c4+" | ");
		System.out.println("laquelle vouler vous choisir? (1,2,3 ou 4 )");
		try {
		cc=sc.nextInt();
		}
		catch(InputMismatchException ime) {
			System.out.println("ceci n'esy pas un valeur numerique, la carte 1 est jou� une valeur numerique");
			cc=0;
		}
		switch (cc) {
		case 1:
			choix =c1;
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.defausse.add(c2);
			this.defausse.add(c3);
			this.defausse.add(c4);
			break;
			
		case 2:
			choix =c2;
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.defausse.add(c1);
			this.defausse.add(c3);
			this.defausse.add(c4);
			break;
			
		case 3:
			choix =c3;
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.defausse.add(c1);
			this.defausse.add(c2);
			this.defausse.add(c4);
			break;
		case 4:
			choix =c4;
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.defausse.add(c1);
			this.defausse.add(c2);
			this.defausse.add(c3);
			break;
		default:
			System.out.println("vous avez rentrer une valeur incorrect la carte numero 1 a �t� jouer");
			choix=c1;
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.pioche.remove(0);
			this.defausse.add(c4);
			this.defausse.add(c2);
			this.defausse.add(c3);
			break;
		}
		
		return choix;
	}
	
	/**
	 * methode : permet le tirage d'une carte sur  > 4
	 * @return Carte : la carte tirer par le joueur sur les > 4 propos�es
	 */
	public Carte tirageMoinde4() {
		int lgt = this.pioche.size();
		int ch;
		Carte res;
		Carte[] tc=new Carte[lgt];
		for (int i = 0; i < lgt; i++) {
			tc[i]=this.pioche.get(i);
			System.out.print(tc[i]);
			System.out.println("");
		}
		System.out.println("quelle carte souhaiter vous choisir? (entre 1 et "+lgt);
		Scanner sc = new Scanner(System.in);
		try {
		ch=sc.nextInt();
		ch=ch-1;
		while(ch<0 || ch>=lgt){
			System.out.println("veuillez choisir un nombre entre 1 et "+lgt);
			ch=sc.nextInt();
			ch=ch-1;
		}
		}
		catch(InputMismatchException ime) {
			System.out.println("ceci n'est pas un nombre , la carte numero 1 � �t� jouee");
			ch=0;
		}
		res=this.pioche.get(ch);
		this.pioche.remove(ch);
		
		return res;
		
	}
	
	public Joueur getJ() {
		return j;
	}


	public int getTypeCycliste() {
		return typeCycliste;
	}


	public void estFatigue(CarteF cf) {
		String aff ="";
		if(this.typeCycliste==1) {
			aff="Le Sprinteur de "+this.getJ().getNom()+" est fatigue";
		}
		else {
			aff="Le rouleur de "+this.getJ().getNom()+" est fatigue";
		}
		System.out.println(aff);
		this.defausse.add(cf);
	}
	@Override
	public String toString() {
		return "Cycliste du " + j + ", de type " + typeCycliste+" de position "+this.pos.getPosi();
	}
	
	
	/**
	 * methode de changement de position du cycliste
	 * @param c : Case qui changera la position du cycliste
	 */
	public void changerPos(Case c) {
		this.pos=c;
		
	}


	public Case getPos() {
		return pos;
	}


	
	
}
