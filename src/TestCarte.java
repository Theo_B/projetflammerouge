import static org.junit.Assert.*;
import java.awt.Color;
import org.junit.Test;


public class TestCarte {
	/**
	 * test le constructeur de CarteE
	 */
	@Test
	public void estConstructeurCarteE() {
		CarteE c=new CarteE(5,3);
		assertEquals("CarteE :le deplacement devrait etre egale a 5",5,c.getDeplacement());
		assertEquals("CarteE :le type du coureur devrait etre egale a 3",3,c.getTypeCoureur());
	}
	/**
	 * test le constructeur de CarteF
	 */
	@Test
	public void estConstructeurCarteF() {
		CarteF c=new CarteF();
		assertEquals("CarteF : le deplacement devrait etre egale a 2",2,c.getDeplacement());
	}

	
}
