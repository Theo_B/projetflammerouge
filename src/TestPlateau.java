import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.Test;

public class TestPlateau {
	/**
	 * test si les tuile se cr�er correctement via la methode creationTuile
	 */
	@Test
	public void testcreationTuile() {
		Plateau p = new Plateau();
		Tuile t= p.creationTuile(0);
		assertEquals("Le type de tuile devrait etre Droit","Droit",t.getTypeTuile());
	}
	/**
	 * test que le m�lange des tuile se fait correctement 
	 */
	@Test
	public void testmelangeTuile() {
		Plateau p = new Plateau();
		ArrayList<Tuile> listetuiletest = p.getListetuile();
		ArrayList<Tuile> listetuileC = new ArrayList<Tuile>(); 
		for(int i=0;i<=7;i++){
			listetuileC.add(p.creationTuile(0));
			if(i>=1&&i<=4){
				listetuileC.add(p.creationTuile(i));
				listetuileC.add(p.creationTuile(i));
			}
		}
		boolean eq=listetuileC.equals(listetuiletest);

		assertEquals("la liste a m�langer n'est pas dans le bon ordre",true,!eq);
		p.melangeTuile();
		boolean res=p.getCircuit().equals(listetuileC);
		assertNotEquals("le Circuit ne devrait pas etre egale a la liste de depart",false,res);
	}

}





