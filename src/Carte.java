/**
 * Class de Carte abstraite
 * @author Theo_B & Geoff_M
 */
public abstract class Carte {
	int deplacement;
	
	public abstract int getDeplacement();
}
