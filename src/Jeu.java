/**
 * Class Jeu
 * @author Theo_B & Geoff_M
 */
import java.util.*;


import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.BufferUnderflowException;


public class Jeu {
	private Plateau plat;
	private ArrayList <CarteF> piocheFC = new ArrayList<CarteF>();
	private ArrayList <CarteF> piocheFS = new ArrayList<CarteF>();
	private Carte[][] tabCarteTour;
	private Joueur[] tabj;
	private int nbj;
	private ArrayList<Cycliste> listeCy;

	/**
	 *Constructeur de Jeu
	 */
	public Jeu() {
		Scanner sc = new Scanner(System.in);
		try {
		System.out.println("Combien y a t'il de joueur de joueur?(Entre 1 et 4)");
		nbj = sc.nextInt();

		while(nbj>4 || nbj <1) {

			System.out.println("le nombre de joueur doit �trer compris entre 1 et 4 ,veuillez recommencer svp");
			nbj = sc.nextInt();
		}
		}
		catch(InputMismatchException ime) {
			System.out.println("Ceci n'est pas une valeur num�rique , vous jouerez donc � 4 joueur");
			nbj=4;
		}
		tabj = new Joueur[nbj];
		listeCy = new ArrayList<Cycliste>();
		plat = new Plateau();

		initialiserJoueur();
		placerCyclisteDepart();

		for (int i = 0; i < nbj; i++) {
			listeCy.add(tabj[i].getRouleur());
			listeCy.add(tabj[i].getSprinter());}


		this.initialiser_pioche();

		this.tabCarteTour = new Carte[nbj][2];
	}
	/**
	 * Constructeur pour les tests
	 * @param p
	 * @param nb
	 * @param lisCy
	 */
	public Jeu(Plateau p,int nb) {
		this.plat = p;
		this.nbj = nb;
		this.listeCy = new ArrayList<Cycliste>();
		this.listeCy.add(new Cycliste());
	}

	/**
	 * methode : initialisation d'une nouvelle pioche 
	 */
	public void initialiser_pioche() {
		this.piocheFC.clear();
		this.piocheFS.clear();
		for (int i = 0; i <60; i++) {
			this.piocheFC.add(new CarteF());
		}

		for (int i = 0; i <60; i++) {
			this.piocheFS.add(new CarteF());
		}
	}


	/**
	 * methode : lancement et suivi du jeu
	 */
	public void jouer() {
		boolean arr=false;
		placerCyclisteDepart();
		AffichagerPiste();
		while(!(arr)) {


			phaseEnergie();
			

			arr=phaseMouvement();
			if(arr) {
				AffichagerPiste();
				terminerJeu();

				break;
			}
			

			
			AffichagerPiste();
			phaseFinale();
			AffichagerPiste();
			
		}
	}
	/**
	 * methode : initialise les joueurs
	 */
	public void initialiserJoueur() {

		System.out.println("Vous jouez � "+nbj+" joueur");
		ArrayList<Color> lcolor = new ArrayList<Color>();
		lcolor.add(Color.blue);
		lcolor.add(Color.red);
		lcolor.add(Color.green);
		lcolor.add(Color.yellow);
		String psd;
		for (int i = 0; i < nbj; i++) {
			Scanner scini = new Scanner(System.in);
			System.out.println("Entrez le pseudo du joueur : "+i);
			psd = scini.nextLine();
			System.out.println("Choisir la couleur de "+psd);


			int l = lcolor.size();
			Color coul;
			System.out.println("indiquer l'index entre 1 et "+l+" de la couleur choisit");
			AfficherChoixCouleur(lcolor);
			int choic;
			
			try {
			choic = scini.nextInt();
			choic=choic-1;
			while(choic<0 || choic >=l) {

				System.out.println("le nombre doit �trer compris entre 1 et "+l+" ,veuillez recommencer svp");
				choic = scini.nextInt();
				choic=choic-1;
			}
			}
			catch(InputMismatchException ime0) {
				System.out.println("ceci n'est pas un nombre la couleur 1 � �t� chosit");
				choic=0;
			}

			coul =lcolor.get(choic);
			lcolor.remove(choic);

			tabj[i]=new Joueur(i,coul,psd,this.plat);

		}
	}

	/**
	 * methode :phaseEnergie: permet le choix des cartes pour chaque cycliste de chaque joueur
	 */
	public void phaseEnergie(){
		System.out.println("Entre dans la phase d'energie");
		for (int i = 0; i < this.nbj; i++) {
			System.out.println(this.tabj[i].getNom()+" (joueur : "+ this.tabj[i].getNum()+" ) choisit sa carte de sprinteur");
			this.tabCarteTour[i][0]=this.tabj[i].getSprinter().choixCarte();
			System.out.println(this.tabj[i].getNom()+" (joueur : "+ this.tabj[i].getNum()+" ) choisit sa carte de rouleur");
			this.tabCarteTour[i][1]=this.tabj[i].getRouleur().choixCarte();	
		}
	}
	/**
	 * methode :phaseMouvement: permet le deplacement des cyclistes
	 * @return	boolean :
	 * 				false si n'est pas arriver
	 * 				true si cycliste est arriver
	 */ 
	public boolean phaseMouvement(){
		boolean res=false;
		System.out.println("Entre dans la phase de mouvement");
		
		classementCycliste();
		
		for (int i = listeCy.size()-1; i >=0; i--) {
			
			
			int numj = listeCy.get(i).getJ().getNum();
			int typcy = listeCy.get(i).getTypeCycliste();
			plat.Deplacement(listeCy.get(i), tabCarteTour[numj][typcy-1].getDeplacement(),false);
			res=etreArrive(listeCy.get(i));
			if(res) {
				break;
			}
		}

		return res;
	}

	public void phaseFinale(){
		System.out.println("Entre dans la phase finale (Application des aspirations)");
		aspiration();
	}

	/**
	 * Mise � jour de l'ordre des cyclistes dans la list
	 */
	public void classementCycliste() {
		ArrayList<Case> lc =this.plat.caseAvecCycliste();
		ArrayList<Cycliste> tmpCy=new ArrayList<Cycliste>();
		
		for (int i = 0; i < lc.size(); i++) {
			Case c = lc.get(i);
			int j =0;
			while(c.getCyD()!=listeCy.get(j)&&c.getCyG()!=listeCy.get(j)) {
				j++;
			}
			tmpCy.add(listeCy.get(j));
			listeCy.remove(j);
		}

		this.listeCy=tmpCy;


	}
	/**
	 * methode qui permet l'aspiration entre les joueur
	 */
	public void aspiration() {
		ArrayList<Case> listePos=this.plat.getListePos();
		for (int i = 0; i < listePos.size()-5; i++) {

			if(!(listePos.get(i).estVideD())) {
				if(listePos.get(i+1).estVideD() && listePos.get(i+2).estVideD()) {
					if(listePos.get(i).getCyD().getTypeCycliste()==1) {
						CarteF cf = this.piocheFS.get(0); 
						listePos.get(i).getCyD().estFatigue(cf);
						this.piocheFS.remove(0);
					}

					else {
						CarteF cf = this.piocheFC.get(0); 
						listePos.get(i).getCyD().estFatigue(cf);
						this.piocheFC.remove(0);
					}
					if(!(listePos.get(i).estVideG())) {
						if(listePos.get(i).getCyG().getTypeCycliste()==1) {
							CarteF cf = this.piocheFS.get(0); 
							listePos.get(i).getCyG().estFatigue(cf);
							this.piocheFS.remove(0);
						}
						else {
							CarteF cf = this.piocheFC.get(0); 
							listePos.get(i).getCyG().estFatigue(cf);
							this.piocheFC.remove(0);
						}
					}
				}
				int j=i;
				while(!(listePos.get(j+1).estVideD())){
					j++;
				}
				if(listePos.get(j+1).estVideD() && !(listePos.get(j+2).estVideD())) {
					for(int k =j ;k>=i;k--) {
						this.plat.Deplacement(listePos.get(k).getCyD(), 1,true);
						if(!(listePos.get(k).estVideG())){
							this.plat.Deplacement(listePos.get(k).getCyG(), 1,true);	
						}
					}
				}
			}
		}

	}
	/**
	 * methode : affiche la position des cycliste
	 */
	public void affichageCycliste() {
		System.out.println("phase d'affichage");
		for(int i = 0 ; i< listeCy.size();i++) {
			int pos = 0;
			
			while(this.plat.getListePos().get(pos).getId()!=listeCy.get(i).getPos().getId()) {
				pos++;
			}
			System.out.println(listeCy.get(i));
		}
	}
	/**
	 * methode d'affichage de la piste
	 */
	public void AffichagerPiste() {	
		System.out.println("[Legende] D : Case D�part |  A : Case arrivee | < :  Case montee | > : case descente | F : Foule(fait gagner ou perdre 1 point de deplacement) ");
		ArrayList<Case> lcase = this.plat.getListePos();
		int l = lcase.size();
		
		for (int i = 0; i < l; i++) {
			String aff = retournerAffichageTypeCase(i);
			System.out.print(aff);
		}
		System.out.println("");
		


		for (int i = 0; i <l; i++) {
			System.out.print("---");
			
		}
		System.out.println("");

		for (int i = 0; i <l; i++) {
			String aff;
			if(!lcase.get(i).estVideG()) {
				int n = lcase.get(i).getCyG().getJ().getNum();
				String tc ="?";
				if(lcase.get(i).getCyG().getTypeCycliste()==1) {
					tc="S";
				}
				else {
					tc="R";
				}
				aff="|"+tc+n;
			}
			else {
				aff="|  ";
			}
			System.out.print(aff);

		}
		System.out.println("");
		for (int i = 0; i < l; i++) {
			System.out.print("===");
		}
		System.out.println("");

		for (int i = 0; i <l; i++) {
			String aff;
			if(!lcase.get(i).estVideD()) {
				int n = lcase.get(i).getCyD().getJ().getNum();
				String tc ="?";
				if(lcase.get(i).getCyD().getTypeCycliste()==1) {
					tc="S";
				}
				else {
					tc="R";
				}
				aff="|"+tc+n;
			}
			else {
				aff="|  ";
			}
			System.out.print(aff);

		}
		System.out.println("");
		for (int i = 0; i <l; i++) {
			System.out.print("---");
			
		}
		System.out.println("");
		for (int i = 0; i < l; i++) {
			String aff = retournerAffichageTypeCase(i);
			System.out.print(aff);
		}
		System.out.println("");

	}

	/**
	 * methode : place les cycliste au d�part du circuit du plateau
	 */
	public void placerCyclisteDepart() {
		ArrayList<Case> listePos=plat.getListePos();
		for (int i = 3; i>=4-nbj; i--) {
			listePos.get(i).placerCycliste(tabj[3-i].getRouleur());
			listePos.get(i).placerCycliste(tabj[3-i].getSprinter());
		}
		
		this.plat.setListePos(listePos);
		
	}

	/**
	 * methode : permet de savoir si le cycliste est arriver a la fin du circuit du plateau
	 * @param c :Cycliste a tester si il est arriver ou non
	 * @return boolean : 
	 * 			true si il est arrive a la fin
	 * 			false si il n'est pas arrive a la fin
	 */
	public boolean etreArrive(Cycliste c) {
		boolean res = false;
		if(c.getPos().getEffet()==222) {
			res=true;
		}
		return res;
	}
	/**
	 * methode d'affichage du type de la case
	 * @param i index de la case a afficher
	 * @return String d'affichage
	 */
	public String retournerAffichageTypeCase(int i) {
		ArrayList<Case> lcase = this.plat.getListePos();
		
		int effet=lcase.get(i).getEffet();
		String aff;
		switch (effet) {
		case 0:
			aff="===";
			break;
			
		case 1:
			aff="<<<";
			break;

			
		case 2:
			aff=">>>";
			break;
			
		case 111:
			aff="DDD";
			break;
		
		case 222:
			aff="AAA";
			break;
		case 3 :
			aff="FFF";
			break;
		default:
			aff="???";
			break;
		}
		return aff;
	}
	
	public void AfficherChoixCouleur(ArrayList<Color> lcolor) {
		for (int i = 0; i < lcolor.size(); i++) {
			Color c = lcolor.get(i);
			int b,r,v;
			b=c.getBlue();
			r = c.getRed();
			v= c.getGreen();
			
			if(b==255 && r==0 && v==0) {
				System.out.print(" [Bleu] ");
			}
			
			if(b==0 && r==255 && v==0) {
				System.out.print(" [Rouge] ");
			}
			
			if(b==0 && r==0 && v==255) {
				System.out.print(" [Vert] ");
			}
			if(b==0 && r==255 && v==255) {
				System.out.print(" [Jaune] ");
			}
			
		}
	}
	/**
	 * methode qui sauvegarde le gagnant 
	 * @param j Joueur a sauvegarder
	 */
	public void sauvegarderGagnant(Joueur j) {
		try {	
		BufferedReader outfich = new BufferedReader(new FileReader("./score.txt"));
		ArrayList<String> lpseudo = new ArrayList<String>();
		String l;
		while((l=outfich.readLine()) != null) {
			
			lpseudo.add(l);

		}
		outfich.close();
		BufferedWriter infich = new BufferedWriter(new FileWriter("./score.txt"));
		for (int i = 0; i < lpseudo.size(); i++) {
			infich.write(lpseudo.get(i));
			infich.newLine();
		}
		infich.write(j.getNom());
		infich.close();
		
		}
		catch(FileNotFoundException fnfe) {
			try {
			BufferedWriter crea = new BufferedWriter(new FileWriter("./score.txt"));
			crea.close();
			sauvegarderGagnant(j);
			}
			catch(Exception e) {
				System.out.println("erreur dans la creation du fichier");
			}
		}
		catch(IOException ioe) {
			System.out.println("erreur IO sur la sauvegarde de score : "+ioe.getMessage());
		}
	}
	/**
	 * methode qui termine le jeux et sauvegarde le gagnant
	 */
	public void terminerJeu() {
		classementCycliste();
		int l=listeCy.size();
		System.out.println("Le cycliste du joueur "+listeCy.get(l-1).getJ().getNom()+" A GAGNER LA COURSE");
		sauvegarderGagnant(listeCy.get(l-1).getJ());
	}
	
}
