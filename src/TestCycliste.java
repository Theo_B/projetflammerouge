import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.*;

public class TestCycliste {
	
	/**
	 * test le constructeur de cycliste
	 */
	@Test
	public void testConstructeurCycliste() {
		Joueur j=new Joueur();
		Cycliste c=new Cycliste(1,j);
		assertEquals("Le type du cycliste devrait etre 1", 1,c.getTypeCycliste());
		assertEquals("Le joueur n'aurait du subir aucune modification", j,c.getJ());
	}
	/**
	 * test de la methode de melange de pioche 
	 */
	@Test
	public void testMelangerPioche() {
		Cycliste c=new Cycliste();
		c.initialiser_pioche(1);
		
		ArrayList<Carte> d=new ArrayList<Carte>((ArrayList<Carte>)(c.getPioche()));
		c.melanger_pioche();
		boolean res=d.equals(c.getPioche());
		
		assertEquals("La pioche avant et apr�s le m�lange devrait faire la meme taille", c.getPioche().size(),d.size());
		assertEquals("La pioche ne devrait pas etre la meme apr�s le m�lange",false,res);
	}
	
	@Test
	public void testtirageMoinde4() {
		Joueur j=new Joueur();
		Cycliste c=new Cycliste(1,j);
	}
	
}
